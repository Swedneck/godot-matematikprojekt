extends CanvasItem

onready var spriteSize = $Sprite.texture.get_size() / 2
onready var Global = $"/root/Global"
# warning-ignore:unused_class_variable
var ScreenCenter = OS.window_size / 2
var BotLeftCorner = Vector2()
var BotRightCorner = Vector2()
var counter = 0


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	print("Window size: ", OS.window_size)
	BotLeftCorner = Vector2(0, OS.window_size.y)
	BotRightCorner = Vector2(OS.window_size.x - spriteSize.x, OS.window_size.y - spriteSize.x)
	$Sprite.position = BotLeftCorner
	Global.gravity = $Input_g.value
	$traj_marker.reset(0.0)


func _draw():
	for position in $coordgrid.posList:
		draw_line(position[0], position[1], Global.GRID, 1.1, true)
	
	draw_polyline(PoolVector2Array($traj_marker.posList), Global.GRAY, 1.1, true)
	draw_polyline(PoolVector2Array($Sprite.posList), Global.WHITE, 1.1, true)


# warning-ignore:unused_argument
func _process(delta):
	if !($Sprite.position.y >= OS.window_size.y + 1) and Global.go:
		$Sprite.posList.append($Sprite.position)
		update()
		$Sprite.speed.y += Global.gravity
	else:
		$Sprite.speed = Vector2(0, 0)
	
	$Sprite.position.y += $Sprite.speed.y + (delta * $Sprite.speed.y)
	$Sprite.position.x += $Sprite.speed.x + (delta * $Sprite.speed.x)
	
	$MousePos.text = "%d,%d" % [get_global_mouse_position().x,
								OS.window_size.y - get_global_mouse_position().y]


func change_gravity(g):
	Global.gravity = g


func go():
	$Sprite.position = BotLeftCorner
	$Sprite.speed.x = $Input_x.value
	$Sprite.speed.y = -$Input_y.value
	$Sprite.posList = [] #Clear the drawn line
	$Sprite.posList.append($Sprite.position)
	Global.go = true
