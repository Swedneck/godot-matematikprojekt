extends Position2D

var line = []
var posList = []

var numLines = 30
var lineHeight = 50


func _ready():
	position.y = OS.window_size.y - lineHeight
	position.x = 0
	for i in range(0, numLines):
		line.append(position)
		position.x = OS.window_size.x
		line.append(position)
		posList.append(line)
		line = []
		position.y -= lineHeight
		position.x = 0
	position.y = 0
	position.x = 0
	for i in range(0, numLines):
		line.append(position)
		position.y = OS.window_size.y
		line.append(position)
		posList.append(line)
		line = []
		position.x += lineHeight
		position.y = 0
	$'..'.update()
