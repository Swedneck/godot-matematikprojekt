extends Position2D

onready var Global = $"/root/Global"

var speed = Vector2()
var posList = []
var min1
var min2


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass


# warning-ignore:unused_argument
func _process(delta):
	pass


func traj():
	position.x += speed.x
	position.y += speed.y
	speed.y += Global.gravity
	posList.append(position)
	$'..'.update()
	while position.y < OS.window_size.y:
		position.x += speed.x
		position.y += speed.y
		speed.y += Global.gravity
		posList.append(position)
		$'..'.update()
	print("min1: %d,%d; min2: %d,%d" % 
		  [0,0, posList[-1].x, OS.window_size.y - posList[-1].y])
	print(posList)
	for pos in posList:
		print(String(pos.x).pad_zeros(3), " ", OS.window_size.y - pos.y)
	var val1 = posList[1]
	var val2 = posList[2]


func hitta_a():
	pass


func reset(value):
	position = $'..'.BotLeftCorner
	posList = [] # Clear the drawn line
	posList.append(position)
	speed.x = $'../Input_x'.value
	speed.y = -$'../Input_y'.value
	traj()
